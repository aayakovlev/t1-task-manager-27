package ru.t1.aayakovlev.tm.model;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface HasCreated {

    @NotNull
    Date getCreated();

    void setCreated(@NotNull final Date created);

}

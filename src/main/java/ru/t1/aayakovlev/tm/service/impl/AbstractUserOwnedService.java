package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.UserOwnedRepository;
import ru.t1.aayakovlev.tm.service.UserOwnedService;

import java.util.Comparator;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends UserOwnedRepository<M>>
        extends AbstractBaseService<M, R> implements UserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final M model = findById(userId, id);
        model.setStatus(status);
    }

    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final int recordCount = repository.count(userId);
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordCount)
            throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final M model = findByIndex(userId, index);
        model.setStatus(status);
    }

    @Override
    public int count(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.count(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        return repository.findAll(userId, comparator);
    }

    @Override
    @NotNull
    @SuppressWarnings({"rawTypes", "unchecked"})
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    @NotNull
    public M findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    @NotNull
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final int recordsCount = repository.count(userId);
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordsCount)
            throw new IndexIncorrectException();
        @Nullable final M model = repository.findByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    @NotNull
    public M remove(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(userId, model);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

    @Override
    @NotNull
    public M removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = findById(userId, id);
        return remove(userId, model);
    }

    @Override
    @NotNull
    public M save(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityEmptyException();
        return repository.save(userId, model);
    }

    @Override
    @NotNull
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final int recordsCount = repository.count(userId);
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordsCount)
            throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final M model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final M model = findByIndex(userId, index);
        model.setName(name);
        model.setDescription(description);
    }

}

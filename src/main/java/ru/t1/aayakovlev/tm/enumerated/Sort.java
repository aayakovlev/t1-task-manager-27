package ru.t1.aayakovlev.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.comparator.StatusComparator;

import java.util.Arrays;
import java.util.Comparator;

public enum Sort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<?> comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        return Arrays.stream(values())
                .filter((s) -> value.equals(s.name()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @SuppressWarnings("rawtypes")
    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

}

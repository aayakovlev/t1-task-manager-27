package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.service.PropertyService;

public final class SystemShowAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String NAME = "about";

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull PropertyService service = getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println("E-mail: " + service.getAuthorEmail());
        System.out.println("Author: " + service.getAuthorName());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("Branch: " + service.getGitBranch());
        System.out.println("Commit id: " + service.getGitCommitId());
        System.out.println("Committer: " + service.getGitCommitterName());
        System.out.println("E-mail: " + service.getGitCommitterEmail());
        System.out.println("Message: " + service.getGitCommitMessage());
        System.out.println("Time: " + service.getGitCommitTime());
    }

}

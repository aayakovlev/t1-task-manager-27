package ru.t1.aayakovlev.tm.command.data;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.Domain;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data to bin file.";

    @NotNull
    private static final String NAME = "data-bin-save";


    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE BINARY]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BIN);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
    }

}

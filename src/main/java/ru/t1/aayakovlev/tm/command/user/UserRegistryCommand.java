package ru.t1.aayakovlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.AuthService;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Register new user.";

    @NotNull
    public static final String NAME = "user-registry";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REGISTRY]");
        System.out.print("Enter login: ");
        @NotNull final String login = nextLine();
        System.out.print("Enter password: ");
        @NotNull final String password = nextLine();
        System.out.print("Enter email: ");
        @NotNull final String email = nextLine();
        @NotNull final AuthService authService = serviceLocator.getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        showUser(user);
    }

}
